TableField Extended
----------
This module is based on TableField module which displays the tabular data.

Input form allows the user (Website administrator) to specify the number of
rows/columns and allows entry into each table cell using text fields and
they are extended with option for any user to dynamically Add New row/New Column or Remove existing row/column.

This module will allow your users to create and insert data in dynamic tables.

How to use it?

After you install the module go to your content type where you want to add
dynamic table and add TableField Extended type field. In field configuration
you can set your initial table size and enable "Add new row" or "Add new column"
or  "Remove row" or "Remove column" option.

INSTALLATION
------------
- Copy tablefield_extended directory to /sites/all/modules
- Enable module at /admin/modules
- Add a tablefield_extended to any entity, for example /admin/structure/types
